package com.fallenpanda.location.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.fallenpanda.location.manager.BaiduLocationManager;

/**
 * 位置信息
 * ============================================================================
 * 版权所有 2014 。
 *
 * @author fallenpanda
 *
 * @version 1.0 2014-12-04
 * ============================================================================
 */
public class MyLocation implements Parcelable {

    public MyLocation(String provider) {
        mProvider = provider;
    }

    public MyLocation(ReverseGeoCodeResult result) {
        this.mProvider = BaiduLocationManager.PROVIDER;
        this.mTime = System.currentTimeMillis();
        this.mCoorType = "bd09ll";
        this.mLatitude = result.getLocation().latitude;
        this.mLongitude = result.getLocation().longitude;
        this.mHasAddress = true;
        this.mAddress = result.getAddress();
        this.mProvince = result.getAddressDetail().province;
        this.mCity = result.getAddressDetail().city;
        this.mDistrict = result.getAddressDetail().district;
    }

    /**
     * 设备标识
     */
    private String mProvider;
    /**
     * 时间
     */
    private long mTime = 0;
    /**
     * 经纬度类型
     */
    private String mCoorType;
    /**
     * 纬度
     */
    private double mLatitude = 0.0;
    /**
     * 经度
     */
    private double mLongitude = 0.0;
    /**
     * 是否有海拔高度
     */
    private boolean mHasAltitude = false;
    /**
     * 海拔高度
     */
    private double mAltitude = 0.0f;
    /**
     * 是否有速度
     */
    private boolean mHasSpeed = false;
    /**
     * 速度
     */
    private float mSpeed = 0.0f;
    /**
     * 是否有精度
     */
    private boolean mHasAccuracy = false;
    /**
     * 精度
     */
    private float mAccuracy = 0.0f;
    /**
     * 是否有地址
     */
    private boolean mHasAddress = false;
    /**
     * 地址
     */
    private String mAddress;
    /**
     * 省
     */
    private String mProvince;
    /**
     * 市
     */
    private String mCity;
    /**
     * 区/县
     */
    private String mDistrict;

    public String getmProvider() {
        return mProvider;
    }

    public void setmProvider(String mProvider) {
        this.mProvider = mProvider;
    }

    public long getmTime() {
        return mTime;
    }

    public void setmTime(long mTime) {
        this.mTime = mTime;
    }

    public String getmCoorType() {
        return mCoorType;
    }

    public void setmCoorType(String mCoorType) {
        this.mCoorType = mCoorType;
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public boolean ismHasAltitude() {
        return mHasAltitude;
    }

    public void setmHasAltitude(boolean mHasAltitude) {
        this.mHasAltitude = mHasAltitude;
    }

    public double getmAltitude() {
        return mAltitude;
    }

    public void setmAltitude(double mAltitude) {
        this.mAltitude = mAltitude;
    }

    public boolean ismHasSpeed() {
        return mHasSpeed;
    }

    public void setmHasSpeed(boolean mHasSpeed) {
        this.mHasSpeed = mHasSpeed;
    }

    public float getmSpeed() {
        return mSpeed;
    }

    public void setmSpeed(float mSpeed) {
        this.mSpeed = mSpeed;
    }

    public boolean ismHasAccuracy() {
        return mHasAccuracy;
    }

    public void setmHasAccuracy(boolean mHasAccuracy) {
        this.mHasAccuracy = mHasAccuracy;
    }

    public float getmAccuracy() {
        return mAccuracy;
    }

    public void setmAccuracy(float mAccuracy) {
        this.mAccuracy = mAccuracy;
    }

    public boolean ismHasAddress() {
        return mHasAddress;
    }

    public void setmHasAddress(boolean mHasAddress) {
        this.mHasAddress = mHasAddress;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmProvince() {
        return mProvince;
    }

    public void setmProvince(String mProvince) {
        this.mProvince = mProvince;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmDistrict() {
        return mDistrict;
    }

    public void setmDistrict(String mDistrict) {
        this.mDistrict = mDistrict;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mProvider);
        dest.writeLong(this.mTime);
        dest.writeString(this.mCoorType);
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeByte(mHasAltitude ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.mAltitude);
        dest.writeByte(mHasSpeed ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.mSpeed);
        dest.writeByte(mHasAccuracy ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.mAccuracy);
        dest.writeByte(mHasAddress ? (byte) 1 : (byte) 0);
        dest.writeString(this.mAddress);
        dest.writeString(this.mProvince);
        dest.writeString(this.mCity);
        dest.writeString(this.mDistrict);
    }

    private MyLocation(Parcel in) {
        this.mProvider = in.readString();
        this.mTime = in.readLong();
        this.mCoorType = in.readString();
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
        this.mHasAltitude = in.readByte() != 0;
        this.mAltitude = in.readDouble();
        this.mHasSpeed = in.readByte() != 0;
        this.mSpeed = in.readFloat();
        this.mHasAccuracy = in.readByte() != 0;
        this.mAccuracy = in.readFloat();
        this.mHasAddress = in.readByte() != 0;
        this.mAddress = in.readString();
        this.mProvince = in.readString();
        this.mCity = in.readString();
        this.mDistrict = in.readString();
    }

    public static final Creator<MyLocation> CREATOR = new Creator<MyLocation>() {
        public MyLocation createFromParcel(Parcel source) {
            return new MyLocation(source);
        }

        public MyLocation[] newArray(int size) {
            return new MyLocation[size];
        }
    };

}
