package com.fallenpanda.location.manager;

import android.os.Bundle;

import com.fallenpanda.location.bean.MyLocation;

/**
 * 基类 - 定位管理器
 * ============================================================================
 * 版权所有 2014 。
 *
 * @author fallenpanda
 *
 * @version 1.0 2014-12-04
 * ============================================================================
 */
public abstract class BaseLocationManager implements ILocationManager {

    /**
     * 事件 - 位置改变
     *
     * @param location 位置信息
     */
    @Override
    public void onLocationChanged(MyLocation location) { }

    /**
     * 事件 - 定位失败
     */
    @Override
    public void onLocationFail(String message){ };

    /**
     * 事件 - 状态改变
     *
     * @param status 状态
     * @param extras 参数
     */
    @Override
    public void onStatusChanged(int status, Bundle extras) { }

    /**
     * 事件 - 设备开启
     */
    @Override
    public void onProviderEnabled() { }

    /**
     * 事件 - 设备关闭
     */
    @Override
    public void onProviderDisabled() { }

    /**
     * 方法 - 修改定位参数
     *
     * @param interval 定位间隔
     * @param isNeedAddress 是否需要地址信息
     */
    @Override
    public void updateLocationOptions(int interval, boolean isNeedAddress) { }

}
