package com.fallenpanda.location.manager;

import android.os.Bundle;

import com.fallenpanda.location.bean.MyLocation;

/**
 * 接口 - 自定义定位接口
 * ============================================================================
 * 版权所有 2014 。
 *
 * @author fallenpanda
 *
 * @version 1.0 2014-12-04
 * ============================================================================
 */
public interface ILocationManager {

    /**
     * 事件 - 位置改变
     *
     * @param location 位置信息
     */
    public void onLocationChanged(MyLocation location);

    /**
     * 事件 - 定位失败
     */
    public void onLocationFail(String message);

    /**
     * 事件 - 状态改变
     *
     * @param status 状态
     * @param extras 参数
     */
    public void onStatusChanged(int status, Bundle extras);

    /**
     * 事件 - 设备开启
     */
    public void onProviderEnabled();

    /**
     * 事件 - 设备关闭
     */
    public void onProviderDisabled();

    /**
     * 更新定位参数
     *
     * @param interval 定位间隔
     * @param isNeedAddress 是否需要地址信息
     */
    public void updateLocationOptions(int interval, boolean isNeedAddress);

}
