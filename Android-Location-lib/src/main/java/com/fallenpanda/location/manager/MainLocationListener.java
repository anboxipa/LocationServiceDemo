package com.fallenpanda.location.manager;

import android.os.Bundle;

import com.fallenpanda.location.bean.MyLocation;

/**
 * 接口 - 定位管理器
 * ============================================================================
 * 版权所有 2014 。
 *
 * @author fallenpanda
 *
 * @version 1.0 2014-12-04
 * ============================================================================
 */
public interface MainLocationListener {

    /**
     * 事件 - 位置改变
     *
     * @param location 位置信息
     */
    public void onLocationChanged(MyLocation location);

    /**
     * 事件 - 定位失败
     *
     * @param provider 设备标识
     */
    public void onLocationFail(String provider, String message);

    /**
     * 事件 - 状态改变
     *
     * @param provider 设备标识
     * @param status 状态
     * @param extras 参数
     */
    public void onStatusChanged(String provider, int status, Bundle extras);

    /**
     * 事件 - 设备开启
     *
     * @param provider 设备标识
     */
    public void onProviderEnabled(String provider);

    /**
     * 事件 - 设备关闭
     *
     * @param provider 设备标识
     */
    public void onProviderDisabled(String provider);

}
