package com.fallenpanda.map;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.fallenpanda.app.android_location_lib.R;
import com.fallenpanda.map.widget.ZoomControls;

/**
 * 地图
 * ============================================================================
 * 版权所有 2015
 *
 * @author fallenpanda
 * @version 1.0 2015/10/26
 * ============================================================================
 */
public class MyMapView extends RelativeLayout implements BaiduMap.OnMapStatusChangeListener {

    private final static float MIN_ZOOMLEVEL = 3;
    private final static float MAX_ZOOMLEVEL = 20;

    private MapView mViewMap;
    private ImageButton mBtnLoc;
    private ZoomControls mZcZoom;

    private BaiduMap mBaiduMap;
    private BaiduMap.OnMapStatusChangeListener onMapStatusChangeListener;

    private float curZoomLevel = 12;//当前缩放级别

    public MyMapView(Context context) {
        this(context, null);
    }

    public MyMapView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyMapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.my_mapview, this, true);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mViewMap = (MapView) findViewById(R.id.map_view);
        mBtnLoc = (ImageButton) findViewById(R.id.map_btn_loc);
        mZcZoom = (ZoomControls) findViewById(R.id.map_zc_zoom);

        initMapView();

        mZcZoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapStatusUpdate u = MapStatusUpdateFactory.zoomTo(curZoomLevel+1);
                mBaiduMap.animateMapStatus(u);
            }
        });
        mZcZoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapStatusUpdate u = MapStatusUpdateFactory.zoomTo(curZoomLevel-1);
                mBaiduMap.animateMapStatus(u);
            }
        });

    }

    private void initMapView() {
        mBaiduMap = mViewMap.getMap();
        mBaiduMap.setOnMapStatusChangeListener(this);
        //隐藏百度logo
        View child = mViewMap.getChildAt(1);
        if (child instanceof ImageView) {
            child.setVisibility(View.INVISIBLE);
        }
        //隐藏缩放控件
        mViewMap.showZoomControls(false);
        //隐藏比例尺
        mViewMap.showScaleControl(false);
    }

    @Override
    public void onMapStatusChangeStart(MapStatus mapStatus) {
        if (onMapStatusChangeListener != null) onMapStatusChangeListener.onMapStatusChangeStart(mapStatus);
    }

    @Override
    public void onMapStatusChange(MapStatus mapStatus) {
        if (onMapStatusChangeListener != null) onMapStatusChangeListener.onMapStatusChange(mapStatus);
    }

    @Override
    public void onMapStatusChangeFinish(MapStatus mapStatus) {
        if (curZoomLevel != mapStatus.zoom)
            onZoomLevelChanged(mapStatus.zoom);
        if (onMapStatusChangeListener != null) onMapStatusChangeListener.onMapStatusChangeFinish(mapStatus);
    }

    private void onZoomLevelChanged(float zoomLevel) {
        this.curZoomLevel = zoomLevel;

        if (curZoomLevel < MIN_ZOOMLEVEL)
            curZoomLevel = MIN_ZOOMLEVEL;
        else if (curZoomLevel > MAX_ZOOMLEVEL)
            curZoomLevel = MAX_ZOOMLEVEL;

        if (curZoomLevel == MIN_ZOOMLEVEL) {
            Toast.makeText(getContext(), "已缩小至最低级别", Toast.LENGTH_SHORT).show();
            mZcZoom.setIsZoomOutEnabled(false);
        } else if (curZoomLevel == MAX_ZOOMLEVEL) {
            Toast.makeText(getContext(), "已放大至最高级别", Toast.LENGTH_SHORT).show();
            mZcZoom.setIsZoomInEnabled(false);
        } else {
            mZcZoom.setIsZoomInEnabled(true);
            mZcZoom.setIsZoomOutEnabled(true);
        }
    }

    public final BaiduMap getMap() {
        return mBaiduMap;
    }

    public final void onDestroy() {
        mViewMap.onDestroy();
    }

    public final void onPause() {
        mViewMap.onPause();
    }

    public final void onResume() {
        mViewMap.onResume();
    }

    public void addView(View child, LayoutParams params) {
        mViewMap.addView(child, params);
    }

    public void removeView(View view) {
        mViewMap.removeView(view);
    }

    public float getCurZoomLevel() {
        return curZoomLevel;
    }

    /**
     * 替代 BadiuNap.setOnMapStatusChangeListener()
     */
    public void setOnMapStatusChangeListener(BaiduMap.OnMapStatusChangeListener onMapStatusChangeListener) {
        this.onMapStatusChangeListener = onMapStatusChangeListener;
    }

    /**
     * 定位按钮
     */
    public void showLocButton(boolean b) {
        mBtnLoc.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    /**
     * 定位按钮点击事件
     */
    public void setOnLocButtonClickListener(OnClickListener listener) {
        mBtnLoc.setOnClickListener(listener);
    }

    /**
     * 缩放控件
     */
    public void showZoomControls(boolean b) {
        mZcZoom.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    /**
     * 比例尺
     */
    public void showScaleControl(boolean b) {
        mViewMap.showScaleControl(b);
    }

    /**
     * 比例尺位置
     */
    public void setScaleControlPosition(Point point) {
        mViewMap.setScaleControlPosition(point);
    }

}
