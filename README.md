#简介

> 初学轻敲，高抬贵手

#定位 Demo

> 采用Gradle构建

##SDK Version

> locSDK_6.13.jar<br>
> baidumapapi_v3_6_0.jar<br>

##Usage
###1.初始化

    private MainLocationManager mainLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //...
        mainLocationManager = MainLocationManager.getInstance();
        //...
        
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainLocationManager.stopBaiduLocation();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        mainLocationManager.registerListener(mainLocationListener);
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        mainLocationManager.unregisterListener(mainLocationListener);
    }
    
    private MainLocationListener mainLocationListener = new MainLocationListener() {
        @Override
        public void onLocationChanged(MyLocation location) {//定位成功
            if (location == null)
                return ;
            //...
        }
    
        @Override
        public void onLocationFail(String provider, String message) { }//定位失败
    
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }//暂可无视
    
        @Override
        public void onProviderEnabled(String provider) { }//暂可无视
    
        @Override
        public void onProviderDisabled(String provider) { }//暂可无视
    };

###2.调用定位方法

> isNeedAddress 是否需要地址信息<br>
> scanSpan 获取位置信息时间间隔（>=1000ms）<br>

注：需要地址信息，没有网络情况下将导致定位失败

获取一次

    mainLocationManager.getBaiduLocationOnce(isNeedAddress);
    
持续获取

    mainLocationManager.getBaiduLocationAuto(scanSpan);//默认不需要地址信息
    mainLocationManager.getBaiduLocationAuto(scanSpan, isNeedAddress);
    
##Screenshots
<img width='540' height='900' src="http://git.oschina.net/595978937/LocationServiceDemo/raw/master/screenshots/S51021-212938.jpg"/><br>
<img width='540' height='900' src="http://git.oschina.net/595978937/LocationServiceDemo/raw/master/screenshots/S51023-171610.jpg"/><br>
<img width='540' height='900' src="http://git.oschina.net/595978937/LocationServiceDemo/raw/master/screenshots/S51023-171649.jpg"/><br>
<img width='540' height='900' src="http://git.oschina.net/595978937/LocationServiceDemo/raw/master/screenshots/S51023-171708.jpg"/><br>
<img width='540' height='900' src="http://git.oschina.net/595978937/LocationServiceDemo/raw/master/screenshots/S51023-171717.jpg"/><br>
